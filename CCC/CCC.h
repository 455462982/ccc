//
//  CCC.h
//  CCC
//
//  Created by tengpan on 2017/8/4.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CCC.
FOUNDATION_EXPORT double CCCVersionNumber;

//! Project version string for CCC.
FOUNDATION_EXPORT const unsigned char CCCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CCC/PublicHeader.h>

#import <CCC/CCCManager.h>
